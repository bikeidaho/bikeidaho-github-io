---
layout: post
title:  "Using Coral Edge as a Trash Can Reminder"
date:   2019-10-02 06:30:42
author: Matt Pasley
categories: [Home Assistant]
tags:	homeassistant, raspberrypi
cover:  "/assets/instacode.png"
---

We have a new sanitation services provider in the neighborhood and with that comes a new trash day; Fridays. Previously, our trash days were Monday and I never missed a day. After the second week and shooting 0% with the new provider, I decided to attempt to solve my problem using the only thing I know how, TECHNOLOGY!
