---
layout: post
title:  "Home Assistant Install Guide for Raspberry Pi 3 B+"
date:   2019-10-02 06:30:42
author: Matt Pasley
categories: [Home Assistant]
tags:	homeassistant, raspberrypi
cover:  "/assets/instacode.png"
---


<p>
<h5>Needed Items:</h5>
<ul>
<li>Raspbeery Pi 3B+</li>
<li>≥8GB MicroSD Card</li>
<li>MicroSD USB Dongle</li>
<li>Ethcher or other means to 'burn an image' to disk</li>
<li>Raspbian-Lite Image on PC with Etcher</li>
</ul><p>



