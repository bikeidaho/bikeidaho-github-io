---
layout: post
title:  "Adventures with Home Assistant"
date:   2019-10-01 08:00:01
author: Matt Pasley
categories: [Homeassistant, DevOps]
tags:	HomeAssistant DevOps
cover:  "/assets/instacode.png"
---

One of my current "obsessions"; Home Assistant, started with me trying to solve a "simple" problem...

<i>How do I automatically set my alarm an hour earlier than 'regularly scheduled' on mornings that it has snowed enough to require shoveling and of course, not change my schedule if no snow removal is needed?<br>
<b>Bonus:</b> Maintain a high level of 'Wife Acceptance Factor' by making the alarm visual and non-wife distrubing.</i>
<p>
A simple problem which I was fairly convinced had already been solved. So, who better to ask than Google. Off to the interwebs I trotted.
<p>
First was <a href="https://ifttt.com" title="IFTTT" target="_blank">IFTTT</a> as an obvious choice because I thought, If <i>Snow</i> then alarm time minus an hour. Made sense, however, I ran into a few obsitcles here.<br>
First was the fact that the weather report was neither accurate nor precise enough for my needs. Additionally, I was on an iOS platform at the time and setting; let alone adjusting, an alarmed seemed daunting at best if not outright impossible. So I set off to solve the "task" problem which led me to tasker.
<p>
Tasker seemed to have promise so I dowloaded the app on my phone and it has pretty much sat in that same state since then. I was too turned off by the precieved complexity that I abandonded tasker before I even started.
<p>
A fun side note: Since this time I have switched to an Android platform and I wanted to give tasker another try so... rinse and repeat. Download app, open it up, scratch my head and abandon.
<p>
What I thought was going to be an easy solution turned into a couple of weekends of frusttration and confusion. Enter Magic Mirror. It had to be cool, it has the word "Magic" in the title.
<p>
Magic mirror IS a cool project but it wasn't what I was looking for at the time. I have infact revisited the Magic Mirror project recently and will post an update soon.
<p>
## HOME ASSISTANT
